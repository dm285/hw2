﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
	public interface IRepository<T>
		where T : BaseEntity
	{
		Task<IEnumerable<T>> GetAllAsync();
		Task<T> GetByIdAsync(Guid id);
		Task<T> CreateAsync(T entity);
		Task<int> DeleteAsync(T entity);
		Task<T> UpdateAsync(T entity);
	}
}